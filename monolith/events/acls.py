from .keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY
import requests
# import json


# GET CONFERENCE LOCATION WEATHER
def get_weather_data(city, state):
    # Use the Open Weather API

    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"

    response = requests.get(url)
    content = response.json()
    # print(content)
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except (KeyError, IndexError):
        return None
    # print(lat, lon)

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    content = response.json()
    # print(content)

    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
    temperature = content["main"]["temp"]
    description = content["weather"][0]["description"]
    weather = {"temperature": temperature, "description": description}
    # print(weather)

    try:
        return weather
    except (KeyError, IndexError):
        return None


# GET LOCATION PICTURES
def get_photo(city, state):
    # Use the Pexels API

    # Create a dictionary for the headers to use in the request
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response

    url = f"https://api.pexels.com/v1/search?query={city}"
    headers = {"Authorization": PEXELS_API_KEY}

    response = requests.get(url, headers=headers)
    content = response.json()

    # print(content)
    try:
        return content["photos"][0]["src"]["original"]
    except (KeyError, IndexError):
        return None
