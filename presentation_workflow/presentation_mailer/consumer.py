import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


# REVISED CODE USING SOLUTION HINTS

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval_message(ch, method, properties, body):
    print("  Received %r" % body)
    python_body = json.loads(body)
    send_mail(
        "Your presentation has been accepted",
        f"{python_body['presenter_name']}, we're happy to tell you that your presentation {python_body['title']} has been accepted",
        "admin@conference.go",
        [{python_body["presenter_email"]}],
        fail_silently=False,
    )


def process_rejection_message(ch, method, properties, body):
    print("  Received %r" % body)
    python_body = json.loads(body)
    send_mail(
        "Your presentation has been rejected",
        f"{python_body['presenter_name']}, we're sorry to inform you that your presentation {python_body['title']} has been rejected",
        "admin@conference.go",
        [{python_body["presenter_email"]}],
        fail_silently=False,
    )


# parameters = pika.ConnectionParameters(host="rabbitmq")
# connection = pika.BlockingConnection(parameters)
# channel = connection.channel()

# channel.queue_declare(queue="presentation_approvals")
# channel.basic_consume(
#     queue="presentation_approvals",
#     on_message_callback=process_approval_message,
#     auto_ack=True,
# )

# channel.queue_declare(queue="presentation_rejections")
# channel.basic_consume(
#     queue="presentation_rejections",
#     on_message_callback=process_rejection_message,
#     auto_ack=True,
# )

# print(" [*] Waiting for approval & rejection messages. To exit press CTRL+C")
# channel.start_consuming()


# NEW CODE --> APPROVALS & REJECTIONS BOTH WORK!
def process_presentation():
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_approvals")
    channel.basic_consume(
        queue="presentation_approvals",
        on_message_callback=process_approval_message,
        auto_ack=True,
    )
    channel.queue_declare(queue="presentation_rejections")
    channel.basic_consume(
        queue="presentation_rejections",
        on_message_callback=process_rejection_message,
        auto_ack=True,
    )
    print(" [*] Waiting for approval or rejection messages. To exit press CTRL+C")
    channel.start_consuming()


# def process_rejection():
#     parameters = pika.ConnectionParameters(host="rabbitmq")
#     connection = pika.BlockingConnection(parameters)
#     channel = connection.channel()
#     channel.queue_declare(queue="presentation_rejections")
#     channel.basic_consume(
#         queue="presentation_rejections",
#         on_message_callback=process_rejection_message,
#         auto_ack=True,
#     )
#     print(" [*] Waiting for rejection messages. To exit press CTRL+C")
#     channel.start_consuming()


# Just extra stuff to do when the script runs

# name-main if statement affects when this function runs;
# only executes if you're running the file directly, not importing as a method
if __name__ == "__main__":
    try:
        process_presentation()
    except KeyboardInterrupt:
        print("Interrupted")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)


# # OLD CODE, CONT'D
# if __name__ == "__main__":
#     try:
#         process_approval()
#         process_rejection()
#     except KeyboardInterrupt:
#         print("Interrupted")
#         try:
#             sys.exit(0)
#         except SystemExit:
#             os._exit(0)


"""
# SOLUTION CODE FROM WARREN

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

connection = pika.BlockingConnection(pika.ConnectionParameters("rabbitmq"))
channel = connection.channel()
channel.queue_declare(queue="presentation_approvals")
channel.queue_declare(queue="presentation_rejections")


def reject(ch, method, properties, body):
    data = json.loads(body)
    email = data["presenter_email"]
    name = data["presenter_name"]
    title = data["title"]
    send_mail(
        "Your presentation has been rejected",
        f"We're sorry, {name}, but your presentation {title} has been rejected",
        "admin@conferece.go",
        [email],
        fail_silently=False,
    )


def approve(ch, method, properties, body):
    data = json.loads(body)
    email = data["presenter_email"]
    name = data["presenter_name"]
    title = data["title"]
    send_mail(
        "Your presentation has been accepted",
        f"We're sorry, {name}, but your presentation {title} has been accepted",
        "admin@conferece.go",
        [email],
        fail_silently=False,
    )


channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=approve,
    auto_ack=True,
)
channel.basic_consume(
    queue="presentation_rejections",
    on_message_callback=reject,
    auto_ack=True,
)
channel.start_consuming()
"""
